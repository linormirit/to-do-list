import { useState } from 'react';
import './App.css'
import List from './projects/List';

function App() {
  const [count, setCount] = useState(0);

  return (
    <> 
      <div className="container">
        <List></List>
      </div>
    </>
  )
}

export default App;
